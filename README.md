# Introduction

This is an application repository for a demo php application. The application is to be deployed in FARGATE instance on AWS and all the logic for infrastructure is to be out of the scope of this repository.
![alt image](https://gitlab.com/ecs-fargate-demo/test-app/tree/master/img/deployment-design.png)

# Build & Deploy Description
The build & deploy steps for this repository has following steps:
* user commits the code in develop branch
* the pipeline pushes the image in the AWS ECR
* infrastructure repo for ecs is imported to run the application in FARGATE instance
* user navigates to [CI job panel](https://gitlab.com/ecs-fargate-demo/test-app/-/jobs/) to retrieve application load balancer instance

# Coveats
* For application specific infrastructure variables please refer [terraform.tfvars](https://gitlab.com/ecs-fargate-demo/test-app/tree/master/terraform.tfvars)

# TODOs
* Reduce build time by running CI build job (currently part of `before_script`) to persist artefacts across all the jobs
* Implement logic for assume-role
* Get rid of keys by implementing authentication via SSO (ex. Okta)
* Make demo app secure on SSL