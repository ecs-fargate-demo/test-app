FROM richarvey/nginx-php-fpm:latest

WORKDIR /etc/nginx

COPY --chown=nginx:nginx ./var/www/html/ /var/www/html/

COPY --chown=nginx:nginx ./etc/nginx/ /etc/nginx/

EXPOSE 80
