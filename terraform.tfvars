# name of the container being hosting application
container_name="test"

# AWS Account number where the FARGATE instance needs to be launched
# aws_account_id=563368307323

# port exposed by the Docker container
# app_port=80

# number of container instances to be run
# app_count=1

# Fargate instance CPU units to be provisioned (1 vCPU = 1024 CPU units)
# fargate_cpu=256

# Fargate instance memmory to be provisioned (in MiB)
# fargate_memory=512

# Docker image to be used for the container
# docker_image="563368307323.dkr.ecr.ap-southeast-2.amazonaws.com/${var.container_name}"

# Name assigned for the container being run
# container_name="test"
