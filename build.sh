#!/bin/sh

# importing variables from terraform config file
TF_CONF_FILE=./terraform.tfvars
IMAGE_TAG="$(echo $CI_BUILD_REF | head -c 8)"

[[ -f $TF_CONF_FILE ]] && . $TF_CONF_FILE

# build and push docker image to ecr
$(aws ecr get-login --no-include-email --region ap-southeast-2)
docker build -t $container_name:${IMAGE_TAG} .
docker tag ${container_name}:${IMAGE_TAG} 563368307323.dkr.ecr.ap-southeast-2.amazonaws.com/${container_name}:${IMAGE_TAG}
docker push 563368307323.dkr.ecr.ap-southeast-2.amazonaws.com/$container_name:${IMAGE_TAG}
